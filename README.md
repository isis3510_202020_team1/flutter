# Somnia

Somnia - Understanding your sleep

![](header.png)

## Features

- [x] Personalized alarms
- [x] Suggested bedtime and wake up hour
- [x] Sleep cycle analysis
- [x] Dreamlog and different sleep activities
- [x] Wake up games
- [x] And more ...

## Requisites

- You should have Android Studio installed on your computer [(You can find instructions here.)](https://developer.android.com/studio).
- You should have the Flutter framework installed on your computer and configured in Android Studio [(You can find instructions here.)](https://flutter.dev/docs/get-started/install?gclid=CjwKCAjw8-78BRA0EiwAFUw8LJjoprL_1_v3440RZiR7A6KSsL0MI8IePe0qnoZzyL0NKKZhfSwP4hoCyPgQAvD_BwE&gclsrc=aw.ds).

## Installation

1. Download and/or open Android Studio. If you have a project already open, close it. You'll be prompted with the following screen.
![Android Studio First Screen Image](https://i.imgur.com/7mFF6nL.png)
2. Select the "Get from version control" option seen in the image found before.
3. Then, select the directory where you want to download the code and enter the project's clone URL in the 'Get from Version Control' screen as seen in the following image. Then press the Clone button.
![Android Studio Clone Image](https://i.imgur.com/AUcnPNS.png)
4. Select "No" if you are asked if you'd like to create a new Android Studio project for the data cloned from git.
![Android Studio No Project Image](https://i.imgur.com/wydF7vE.png)
5. You'll be back at the first screen found here, but now, you'll have to select the 'Open an existing Android Project' option in this screen, and then select the folder where you decided to download the code on step 3.
6. Now the code will be opened in the Android Studio IDE as a Flutter project succesfully. At first you may see multiple errors as seen in the bottom of the following screen. We'll take care of that in a further step.
7. Right now you are in the master branch, but for now we are working in the development branch, which means you'd need to checkout to the development branch. To do this, we first need to fetch the branches by going to VCS > Git > Fetch as seen in the following screen.
![Android Studio Fetch Image](https://i.imgur.com/fHWsTKp.png)
8. After fetching, you should check out by going to VCS > Git > Branches. You'll see a little pop up show up in your screen where you'll find the different branches that are actually in the git project. As of now, we are interested on the 'development' branch, so go ahead and click on it and then click on checkout.
![Android Studio Branches Image](https://i.imgur.com/F4Lx3uc.png)
9. Great! Now we are in the development branch. Remember the errors we talked about on step 6? There may even be more now! This is because dependencies haven't been downloaded. To download them, go to the pubspec.yaml file and in the top bar of the code screen click 'Pub get'.
![Android Studio pubspec Image](https://i.imgur.com/UeCSgYB.png)
10. After this process finishes, you should not see any kind of errors. However, sometimes Android Studio fails when recognizing the downloaded dependencies. If there's any error left, we suggest you to close and reopen the project in Android Studio.
11. Now we are ready to run the code. For this, Android Studio provides us with different options at the top bar. You'll see that there's a little dropdown box that says 'no devices' right next to the dropdown box that says 'main.dart' as seen on the following screen.
![Android Studio top bar Image](https://i.imgur.com/8jclKXh.png)
12. If you wanna use an emulator, before being able to run the app, you need to configure at least one AVD on Android Studio. [Here you can find a tutorial to do it Image](https://developer.android.com/studio/run/managing-avds). You can also use a real device by connecting it to your computer. After having connected your device or created a new AVD, you can go to the 'no devices' dropdown and click on it. You'll find a list of the available devices to run the app in as seen in the following screen. Select your connected device or your previously configured AVD. _(NOTE: If you are a MacOS user, don't select the 'Open iOS Simulator' option. You may be able to run the app on iOS but it won't really be functional as some of the most important changes have only been done for the Android platform.)_
![Android Studio top bar Image](https://i.imgur.com/JMlwvsm.png)
13. And, finally, that's it! When your AVD finishes initiallizing you can go ahead and press the run/play button found right next to the 'main.dart' dropown.
